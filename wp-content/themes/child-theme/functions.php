<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


function recent_post_shortcode($number){
	$a = shortcode_atts( array(
		'number' => 5
	), $number );
	$html = "<ul>";
    $recent_posts = wp_get_recent_posts(array('post_type'=>'film'));
    $i = 0;
    foreach( $recent_posts as $recent ){
    	$i++;
    	if($i<=$a['number'])
        $html .= '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
    }
    $html .= "</ul>";
   return $html;
}
add_shortcode( 'recent_post', 'recent_post_shortcode' );


function sb_get_taxonomy($id,$name){
	
	$terms = wp_get_post_terms( $id,$name); 

	$string = "";

	if(count($terms)>1){
		foreach ($terms as $key => $value) {
			if($key == 0)
			$string .= $value->name;
			$string .= ", ".$value->name;
		}

	}else{
		if(count($terms)) $string = $terms[0]->name;
	}

	return $string;
}
