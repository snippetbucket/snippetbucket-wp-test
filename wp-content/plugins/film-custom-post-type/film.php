<?php
/*
Plugin Name: Film Custom Post Types
Plugin URI: http://snippetbucket.com
Description: Custom Post Types for "Films".
Version: 5.0
Author: snippetbucket
Author URI: http://snippetbucket.com
License: GPL2
*/

add_action( 'init', 'film_custom_post_type' );

function film_custom_post_type() {
// Genre, Country, Year and Actors
flush_rewrite_rules( false );
register_post_type( 'film', array(
  'labels' => array(
    'name' => 'Films',
    'singular_name' => 'Film',
	'all_items'          => __( 'All Films' ),
   ),
  'description' => 'Film which we will add as new custom post type.',
  'public' => true,
  'show_in_menu' => true,
  'show_in_nav_menus' => true,
  'show_ui' => true,
  'has_archive' => true,
  'menu_position' => 2,
  'taxonomies' => array('genre', 'country', 'year', 'actors' ),
  'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
  'rewrite' => array('slug' => 'films'),
));
}