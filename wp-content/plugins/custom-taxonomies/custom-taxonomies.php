<?php
/*
Plugin Name: Film Custom Taxonomies
Plugin URI: http://snippetbucket.com
Description: Custom Taxonomies for Post Type "Film".
Version: 5.0
Author: snippetbucket
Author URI: http://snippetbucket.com

*/


function film_taxonomies() {
// Genre, Country, Year and Actors

    $args = array(
        'label' => __( 'Genre' ),
        'rewrite' => array( 'slug' => 'genre' ),
        'hierarchical' => true,
        'show_admin_column' => true,
    );

    register_taxonomy( 'genre', 'film', $args );

    $args = array(
            'label' => __( 'Country' ),
            'rewrite' => array( 'slug' => 'country' ),
            'hierarchical' => true,
            'show_admin_column' => true,
        );

    register_taxonomy( 'country', 'film', $args );

    $args = array(
            'label' => __( 'Year' ),
            'rewrite' => array( 'slug' => 'year' ),
            'hierarchical' => true,
            'show_admin_column' => true,
        );

    register_taxonomy( 'year', 'film', $args );

    $args = array(
            'label' => __( 'Actors' ),
            'rewrite' => array( 'slug' => 'actors' ),
            'hierarchical' => true,
            'show_admin_column' => true,
        );

    register_taxonomy( 'actors', 'film', $args );

}
add_action( 'init', 'film_taxonomies' );