<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'sb@mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.)Gy/*jBS?OF;KFhQgP ah<ERji*$IyOQjnZHb $.&+_P./FQOg&}TRK@h8}=!/2');
define('SECURE_AUTH_KEY',  'I{D<&-g+],Y<_,do/aK4>Toqi/R?!TDTsxtBj`Sod=,-H.za)Ux=vbhz5PGAlB+p');
define('LOGGED_IN_KEY',    'u!,q.&JJhHI[@w[!ON7<Wq``9.-I.D5 4rR1Kg6-w_li2|&4fn@E6-Mft_9q!O=j');
define('NONCE_KEY',        'GxO)|`RC)&ZKkc]p6T^IAnbx$:DIH^+0.&qt7%u(q$VfyG!;>n)H{ BPm0%GW b]');
define('AUTH_SALT',        '%>UA)}ge$1*GU?.C@(&>ej[:Wl@+~ Bv.b7Hqg&U&soJ Ma{J=<#@$.)*E*+JX+z');
define('SECURE_AUTH_SALT', 'lTj:y5G*IPyX-jslBNtEVzD;CoTz$azn`b?};~]P3r;0}LZ%s+(0)O+$#0WqrKU+');
define('LOGGED_IN_SALT',   ' IX=7_6]^brsWGLP)*JOo!d$y7:Nnj-y015]^nY|gB,)Z@CYtvke63~)[Ae??s@s');
define('NONCE_SALT',       'h{cz:zNKQHdb>9 ^S@&D]7B}m-?*hJ;f9=TpJOUj;M(Qti,WN(l/>5`Vpm3E[zlY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
